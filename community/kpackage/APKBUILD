# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpackage
pkgver=5.102.0
pkgrel=0
pkgdesc="Framework that lets applications manage user installable packages of non-binary assets"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev doxygen samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring installed Plasma, which causes a circular dependency

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
d24be8359eae24e735e740f821bad70e5050e4348d578ff574050da315ceae47f902b0260e0ac22fdefaf412a945fb6bc2d9beac3a6443b42257ed1bb3340c54  kpackage-5.102.0.tar.xz
"
