# Maintainer: Hugo Osvaldo Barrera <hugo@whynothugo.nl>
pkgname=ruff
pkgver=0.0.223
pkgrel=0
pkgdesc="Extremely fast Python linter"
url="https://github.com/charliermarsh/ruff"
# ppc64le, s390x, riscv64: ring
# x86, armhf, armv7: fails tests on 32-bit
arch="all !x86 !armhf !armv7 !ppc64le !s390x !riscv64"
license="MIT"
makedepends="maturin cargo py3-installer"
source="$pkgname-$pkgver.tar.gz::https://github.com/charliermarsh/ruff/archive/refs/tags/v$pkgver.tar.gz"

export CARGO_PROFILE_RELEASE_OPT_LEVEL=2

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	maturin build --release --frozen --manylinux off
}

check() {
	cargo test --frozen
}

package() {
	python3 -m installer -d "$pkgdir" \
		target/wheels/*.whl
}

sha512sums="
863522918e8c01fc2926adb67b8a77fa9b7bcbf8c88e9a418eca64f63856307f21f5af93a783246ced26024a3e91ab965f4289a00daf80906f27c7945eb23911  ruff-0.0.223.tar.gz
"
