# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=diffoscope
pkgver=232
pkgrel=0
pkgdesc="In-depth comparison of files, archives, and directories"
url="https://diffoscope.org/"
arch="noarch"
license="GPL-3.0-or-later"
depends="
	py3-libarchive-c
	py3-magic
	python3
	"
makedepends="
	py3-docutils
	py3-setuptools
	python3-dev
	"
checkdepends="
	bzip2
	cdrkit
	gzip
	libarchive-tools
	openssh-client-default
	py3-html2text
	py3-pytest
	unzip
	"
source="https://salsa.debian.org/reproducible-builds/diffoscope/-/archive/$pkgver/diffoscope-$pkgver.tar.gz"

build() {
	python3 setup.py build
}

check() {
	# html test fails
	PYTHONPATH="$PWD" \
	PYTHONDONTWRITEBYTECODE=1 \
	pytest -v -k 'not test_diff'
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
858073c70fcd439de8839dc4e7c0083c839353a00e723dd5dd8ecc5b955de8c2012fa412dfcd7fa90018fb199182cb27727be131fe26aaa5cf702d76fa7fdbdb  diffoscope-232.tar.gz
"
