# Contributor: Aiden Grossman <agrossman154@yahoo.com>
# Maintainer: Aiden Grossman <agrossman154@yahoo.com>
pkgname=py3-jupyter_core
pkgver=5.1.3
pkgrel=0
pkgdesc="Core Jupyter functionality"
url="https://github.com/jupyter/jupyter_core"
arch="noarch"
license="BSD-3-Clause"
depends="py3-traitlets py3-platformdirs"
makedepends="py3-hatchling py3-gpep517"
checkdepends="py3-pytest"
source="$pkgname-$pkgver.tar.gz::https://github.com/jupyter/jupyter_core/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/jupyter_core-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest \
		--deselect jupyter_core/tests/test_command.py::test_not_on_path \
		--deselect jupyter_core/tests/test_command.py::test_path_priority \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_prefer_env \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_user_site \
		--deselect jupyter_core/tests/test_paths.py::test_jupyter_path_no_user_site \
		--deselect jupyter_core/tests/test_command.py::test_argv0
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/jupyter_core-$pkgver-py3-none-any.whl
}

sha512sums="
f51b094e5b13566ebdd1493a9a7646c0ba652c348987994d5cd95028ae981f05ce3659d23b14b8e0c886e7eb0e0410d4c40aef4ae5344ca2655b2f8d0f961d37  py3-jupyter_core-5.1.3.tar.gz
"
