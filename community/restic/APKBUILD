# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer: Sören Tempel <soeren+alpine@soeren-tempel.net>
pkgname=restic
pkgver=0.15.0
pkgrel=0
pkgdesc="Fast, secure, efficient backup program"
url="https://restic.net/"
arch="all"
license="BSD-2-Clause"
makedepends="go"
options="net chmod-clean"
subpackages="$pkgname-doc $pkgname-bash-completion $pkgname-zsh-completion"
source="https://github.com/restic/restic/releases/download/v$pkgver/restic-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -v ./cmd/...
}

check() {
	RESTIC_TEST_FUSE=0 make test
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname

	local man
	for man in doc/man/*.?; do
		install -Dm644 "$man" \
			"$pkgdir"/usr/share/man/man${man##*.}/${man##*/}
	done

	install -Dm644 doc/bash-completion.sh \
		"$pkgdir"/usr/share/bash-completion/completions/$pkgname
	install -Dm644 doc/zsh-completion.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_$pkgname
}

sha512sums="
d951c847fff5314f6db49df83702f26ff32bcc765226ebae63008aa84cda1d1e5e7a47347f92774fab9662b9a618354bde26a0ec7eea95591a414978dd754007  restic-0.15.0.tar.gz
"
