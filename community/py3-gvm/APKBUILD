# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=py3-gvm
_pkgname=${pkgname/py3/python}
pkgver=22.9.0
pkgrel=2
pkgdesc="Greenbone Vulnerability Management Python Library "
url="https://github.com/greenbone/python-gvm"
arch="noarch"
license="GPL-3.0-or-later"
makedepends="py3-gpep517 py3-installer py3-poetry-core"
checkdepends="py3-defusedxml py3-pytest py3-lxml py3-paramiko"
source="$pkgname-$pkgver.tar.gz::https://github.com/greenbone/$_pkgname/archive/v${pkgver/_/.}.tar.gz"
builddir="$srcdir/$_pkgname-${pkgver/_/.}"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	# hangs
	testenv/bin/python3 -m pytest \
		--deselect=tests/connections/test_ssh_connection.py
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
9f97cc4f358f2aba8b6678245cca0a37a627a8fb2789c444bd6673d4e984da9cb0522c50913f3d2cd9f85cf2246eac77dbbe647c93ca0df9d616040279787bbe  py3-gvm-22.9.0.tar.gz
"
